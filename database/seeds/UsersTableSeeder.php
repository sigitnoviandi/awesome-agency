<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\User::class, 2)->create()->each(function($user){
            $user->categories()->saveMany(factory(App\Models\Category::class, 4)->make());

            // 3rd nest seeding
            $user->categories()->each(function($category) use($user){
                $category->posts()->saveMany(factory(App\Models\Post::class, 4)->make(['user_id' => $user->id]));
            });
        });
        
    }
}
