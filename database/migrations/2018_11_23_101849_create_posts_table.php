<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id')->autoIncrement();
            $table->unsignedInteger('user_id');
            $table->text('title');
            $table->string('slug', 191)->unique();
            $table->text('excerpt')->nullable(true);
            $table->longText('content');
            $table->text('featured_image')->nullable(true);
            $table->text('thumbnail')->nullable(true);
            $table->string('status');
            $table->dateTime('published_at')->nullable(true);
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
