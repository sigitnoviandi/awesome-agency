<?php

use Faker\Generator as Faker;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\Post::class, function (Faker $faker) {
    $post_title = $faker->unique()->sentence(7);
    $post_slug = str_slug($post_title);
    
    $file_path = storage_path('app/public/images');
    
    if(File::exists($file_path) == FALSE){
        File::makeDirectory($file_path);  //follow the declaration to see the complete signature
    }
    $featured_image = $faker->image($file_path, 1920, 1080);
    $featured_image_filename = basename($featured_image);
    
    $thumbnail_file_path = storage_path('app/public/thumbnails');
    
    if(File::exists($thumbnail_file_path) == FALSE){
        File::makeDirectory($thumbnail_file_path);  //follow the declaration to see the complete signature
    }
    $thumbnail_image = $img = Image::make($featured_image)->resize(800, NULL, function($constraint){
        $constraint->aspectRatio();
    });
    $thumbnail_image->save($thumbnail_file_path . '/' . $featured_image_filename);
    
    $post = [
        'title' => ucfirst($post_title),
        'slug' => $post_slug,
        'excerpt' => implode(' ', $faker->sentences(4)),
        'content' => '<p>' . implode('</p><p>', $faker->paragraphs(7)) . '</p>',
        'featured_image' => $featured_image_filename,
        'thumbnail' => $featured_image_filename,
        'status' => 'published',
        'published_at' => date('Y-m-d H:i:s')
    ];
    
    return $post;
});
