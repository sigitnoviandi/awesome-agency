<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\Category::class, function (Faker $faker) {
    $default_categories = ['Health', 'Religion', 'Finance', 'Sport', 'Politics', 'Travel', 'Technology', 'Food', 'People', 'Culture', 'International', 'National'];
    $category_name = $faker->unique()->randomElement($default_categories);
    $category_slug = str_slug($category_name);
    
    return [
        'name' => ucfirst($category_name),
        'slug' => $category_slug,
        'created_at' => date('Y-m-d'),
        'updated_at' => date('Y-m-d')
    ];
});
