<?php
// Home
Breadcrumbs::register('dashboard', function($breadcrumbs){
    $breadcrumbs->push('Dashboard', route('admin.dashboard'));
});

// Posts
Breadcrumbs::register('manage_posts', function($breadcrumbs){
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Posts', route('admin.post.manage'));
});

Breadcrumbs::register('edit_post', function($breadcrumbs, $post_id){
    $breadcrumbs->parent('manage_posts');
    $breadcrumbs->push('Edit', route('admin.post.edit', [$post_id]));
});

Breadcrumbs::register('add_post', function($breadcrumbs){
    $breadcrumbs->parent('manage_posts');
    $breadcrumbs->push('Add', route('admin.post.add'));
});

// Category
Breadcrumbs::register('manage_categories', function($breadcrumbs){
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Categories', route('admin.category.manage'));
});

Breadcrumbs::register('edit_category', function($breadcrumbs, $category_id){
    $breadcrumbs->parent('manage_categories');
    $breadcrumbs->push('Edit', route('admin.category.edit', [$category_id]));
});

Breadcrumbs::register('add_category', function($breadcrumbs){
    $breadcrumbs->parent('manage_categories');
    $breadcrumbs->push('Add', route('admin.category.add'));
});

// Profile
Breadcrumbs::register('edit_profile', function($breadcrumbs){
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Edit Profile', route('admin.user.profile.edit'));
});