<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as' => 'home.index', 'uses' => 'Front\HomepageController@index']);
Route::get('article/{slug}', ['as' => 'article.single', 'uses' => 'Front\ArticleController@index']);
Route::get('category/{slug}', ['as' => 'article.list_by_category', 'uses' => 'Front\ArticleController@list_by_category']);

Route::group(['middleware' => 'guest'], function () {
    Route::get('auth/socialite/{social_media}', 'Auth\LoginController@redirect_to_provider');
    Route::get('auth/socialite/callback/{social_media}', 'Auth\LoginController@handle_provider_callback');
});

Route::group(['middleware' => 'auth'], function () {
    ## post routes
    Route::get('admin/dashboard', ['as' => 'admin.dashboard', 'uses' => 'HomeController@index']);
    Route::get('admin/posts/manage', ['as' => 'admin.post.manage', 'uses' => 'Back\PostController@index']);
    Route::get('admin/post/add', ['as' => 'admin.post.add', 'uses' => 'Back\PostController@add']);
    Route::get('admin/post/edit/{id}', ['as' => 'admin.post.edit', 'uses' => 'Back\PostController@edit']);
    Route::post('admin/post/save', ['as' => 'admin.post.save', 'uses' => 'Back\PostController@save']);
    Route::get('admin/post/remove/{id}', ['as' => 'admin.post.remove', 'uses' => 'Back\PostController@remove']);
    Route::post('admin/post/set_filter', ['as' => 'admin.post.filter.set', 'uses' => 'Back\PostController@set_filter']);
    Route::get('admin/post/clear_filter', ['as' => 'admin.post.filter.clear', 'uses' => 'Back\PostController@clear_filter']);
    
    ## category routes
    Route::get('admin/categories/manage', ['as' => 'admin.category.manage', 'uses' => 'Back\CategoryController@index']);
    Route::get('admin/category/add', ['as' => 'admin.category.add', 'uses' => 'Back\CategoryController@add']);
    Route::get('admin/category/edit/{id}', ['as' => 'admin.category.edit', 'uses' => 'Back\CategoryController@edit']);
    Route::post('admin/category/save', ['as' => 'admin.category.save', 'uses' => 'Back\CategoryController@save']);
    Route::get('admin/category/remove/{id}', ['as' => 'admin.category.remove', 'uses' => 'Back\CategoryController@remove']);
    Route::post('admin/category/set_filter', ['as' => 'admin.category.filter.set', 'uses' => 'Back\CategoryController@set_filter']);
    Route::get('admin/category/clear_filter', ['as' => 'admin.category.filter.clear', 'uses' => 'Back\CategoryController@clear_filter']);
    
    ## user routes
    Route::get('admin/user/profile/edit', ['as' => 'admin.user.profile.edit', 'uses' => 'Back\UserController@profile_form']);
    Route::post('admin/user/profile/save', ['as' => 'admin.user.profile.save', 'uses' => 'Back\UserController@profile_save']);
});
