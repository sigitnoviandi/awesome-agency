@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
    Update Profile
@endsection

@section('contentheader_title')
    Update Profile
@endsection

@section('contentheader_description')
    Update your profile
@endsection

@section('page_breadcrumbs')
    {!! Breadcrumbs::render('edit_profile') !!}
@endsection

@section('custom-css')
@endsection

@section('main-content')
<!-- page buttons -->
<div class="row">
    <div class="col-lg-"></div>
</div>
<!-- end of page buttons -->

<!-- alert -->
@if(count(Alert::get()) > 0)        
    @foreach (Alert::get() as $alert)
        <div class="alert alert-{{ $alert->class }} alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <p>{{ $alert->message }}</p>
        </div>
    @endforeach                    
@endif
<!-- end of alert -->

<!-- error -->
@if (count($errors) > 0)
<div class="alert alert-danger">
    <strong>Error!</strong> {{ trans('adminlte_lang::message.someproblems') }}<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<!-- page table -->
@include('backend.user.partials.profile_form')
<!-- end of page table -->
@endsection

@section('custom-js')
<script src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>
<script src="{{ asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<script src="{{ asset('plugins/select2/select2.full.min.js') }}"></script>

<script src="{{ asset('js/user.min.js') }}"></script>
@endsection