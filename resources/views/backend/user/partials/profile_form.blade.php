<!-- general form elements -->
<!-- form start -->
<form role="form" id="form_profile" action="{{ URL::Route('admin.user.profile.save') }}" method="POST" class="profile-form">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-default">
                <div class="box-body action-wrapper">  
                    <span>Changes will NOT be saved before you click the Save button.</span>
                    <a class="btn btn-danger pull-right" id="btn_reset">Reset</a>
                    <a class="btn btn-default pull-right" href="{{ URL::Route('admin.dashboard') }}" id="btn_cancel">Cancel</a>
                    <a class="btn btn-primary pull-right" id="btn_save">Save</a>
                    <span id="loading-indicator" class="loading-indicator pull-right inactive"><i class="fa fa-spinner fa-spin fa-pulse fa-lg"></i></span>
                </div>
            </div>
        </div>
    </div>

    <!-- form start -->
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        Update Profile
                    </h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group has-feedback">
                        <label for="name">Name *</label>
                        <input type="text" class="form-control" placeholder="Your Name" name="name" id="name" value="{{ isset($user->name) ? $user->name : old('name') }}"/>
                    </div>
                    <div class="form-group has-feedback">
                        <label for="slug">Email</label>
                        <input type="email" class="form-control" placeholder="Your Email Address" name="email" id="email" value="{{ isset($user->email) ? $user->email : old('email') }}"/>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        Change Password
                    </h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group has-feedback">
                        <label for="old_password">Old Password</label>
                        <input type="password" class="form-control" placeholder="Your old password" name="old_password" id="old_password" value=""/>
                    </div>
                    <div class="form-group has-feedback">
                        <label for="new_password">New Password</label>
                        <input type="password" class="form-control" placeholder="Your new password" name="new_password" id="new_password" value=""/>
                    </div>
                    <div class="form-group has-feedback">
                        <label for="new_password_confirmation">Confirm New Password</label>
                        <input type="password" class="form-control" placeholder="Confirm your new password" name="new_password_confirmation" id="new_password_confirmation" value=""/>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>    
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
</form>