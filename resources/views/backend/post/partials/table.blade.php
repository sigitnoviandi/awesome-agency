<div class="box table-container">
    <div class="box-header">
        Show 
        <select name="limit" id="filter-limit-options" class="limit-options">
            <option value="10" @if(isset($filter['filter_limit']) && $filter['filter_limit'] == 10) selected @endif>10</option>
            <option value="20" @if(isset($filter['filter_limit']) && $filter['filter_limit'] == 20) selected @endif>20</option>
            <option value="40" @if(isset($filter['filter_limit']) && $filter['filter_limit'] == 40) selected @endif>40</option>
            <option value="50" @if(isset($filter['filter_limit']) && $filter['filter_limit'] == 50) selected @endif>50</option>
            <option value="100" @if(isset($filter['filter_limit']) && $filter['filter_limit'] == 100) selected @endif>100</option>
        </select> baris
        <div class="box-tools">
            @include('backend.pagination.default', ['paginator' => $posts])
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body no-padding">
        <form id="filter-form" method="post" action="{{ URL::Route('admin.post.filter.set') }}">            
            <table class="table table-striped table-hover table-bordered">
                <tr>
                    <th style="width: 2%;">#</th>
                    <th style="width: 25%;">Title</th>
                    <th style="width: 15%;">Category</th>
                    <th style="width: 15%;">Author</th>
                    <th style="width: 10%;">Status</th>
                    <th style="width: 15%;">Published At</th>
                    <th style="width: 15%;">&nbsp;</th>
                </tr>
                <tr class="filter">
                    <td>&nbsp;</td>
                    <td><input type="text" name="filter_title" class="form-control input-sm" placeholder="Search Title" value="{{ isset($filter['filter_title']) ? $filter['filter_title'] : '' }}"/></td>
                    <td><input type="text" name="filter_category" class="form-control input-sm" placeholder="Search Category" value="{{ isset($filter['filter_category']) ? $filter['filter_category'] : '' }}" id="filter_category"/></td>
                    <td><input type="text" name="filter_author" class="form-control input-sm" placeholder="Search Author" value="{{ isset($filter['filter_author']) ? $filter['filter_author'] : '' }}" id="filter_author"/></td>
                    <td>
                        <select name="filter_status" class="form-control input-sm" id="filter_status">
                            <option value="">All</option>
                            <option value="draft" @if(isset($filter['filter_status']) && $filter['filter_status'] == 'draft') selected @endif>Draft</option>
                            <option value="published" @if(isset($filter['filter_status']) && $filter['filter_status'] == 'published') selected @endif>Published</option>
                        </select>
                    </td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                @if(isset($posts) && !empty($posts))
                    @foreach($posts as $post)
                        <tr>
                            <td>{{ $row_no++ }}</td>
                            <td><a href="{{ URL::Route('admin.post.edit', [$post->id]) }}" title="Edit this Article">{{ $post->title }}</a></td>
                            <td>
                                @if(count($post->categories) > 0)
                                    @foreach($post->categories as $category)
                                        <a class="label label-default post-category" data-category="{{ $category->name }}">{{ $category->name }}</a>
                                    @endforeach
                                @endif
                            </td>
                            <td><a class="post-author" data-author="{{ $post->user->name }}">{{ $post->user->name }}</a></td>
                            <td>
                                @if($post->status == 'draft')
                                    <a class="label label-default post-status" data-status="draft">Draft</a>
                                @elseif($post->status == 'published')
                                    <a class="label label-success post-status" data-status="published">Published</a>
                                @endif
                            </td>
                            <td>
                                {{ $post->published_at() }}
                            </td>
                            
                            <td class="action-buttons">
                                <a href="{{ URL::Route('admin.post.remove', [$post->id]) }}" class="btn btn-danger btn-sm btn-remove" data-record-name="{{ $post->title }}" title="Remove Article"><i class="fa fa-trash"></i></a>
                                <a href="{{ URL::Route('admin.post.edit', [$post->id]) }}" class="btn btn-default btn-sm" title="Edit Article"><i class="fa fa-pencil"></i></a>
                                <a href="{{ URL::Route('article.single', [$post->slug]) }}" class="btn btn-default btn-sm" title="View Article" target="_blank"><i class="fa fa-eye"></i></a>
                            </td>
                        </tr>
                    @endforeach
                @else
                <tr>
                    <td colspan="6">There is no article yet.</td>
                </tr>
                @endif
            </table>
            <input type="hidden" value="10" name="filter_limit" id="filter-limit"/>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
        </form>
    </div>
    <!-- /.box-body -->
    <div class="box-footer clearfix">
        <span class="no-of-records">Showing {{ $first_record_no }}&ndash;{{ $row_no - 1 }} of {{ number_format($posts->total()) }} rows.</span>
        @include('backend.pagination.default', ['paginator' => $posts])
    </div>
</div>
<!-- /.box -->