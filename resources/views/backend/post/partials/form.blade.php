<!-- general form elements -->
<!-- form start -->
<form role="form" id="form_post" action="{{ URL::Route('admin.post.save') }}" method="POST" class="article-form" enctype="multipart/form-data">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-default">
                <div class="box-body action-wrapper">  
                    <span>Changes will NOT be saved before you click the Save button.</span>
                    <a class="btn btn-danger pull-right" id="btn_reset">Reset</a>
                    <a class="btn btn-default pull-right" href="{{ URL::Route('admin.post.manage') }}" id="btn_cancel">Cancel</a>
                    <a class="btn btn-primary pull-right" id="btn_save">Save</a>
                    <span id="loading-indicator" class="loading-indicator pull-right inactive"><i class="fa fa-spinner fa-spin fa-pulse fa-lg"></i></span>
                </div>
            </div>
        </div>
    </div>

    <!-- form start -->
    <div class="row">
        <div class="col-lg-9 col-md-9">
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        @if(isset($post) && !empty($post))
                            Edit Article
                        @else
                            Add a New Article
                        @endif
                    </h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group has-feedback">
                        <label for="title">Title *</label>
                        <input type="text" class="form-control" placeholder="Article Title" name="title" id="title" value="{{ isset($post->title) ? $post->title : old('title') }}"/>
                    </div>
                    <div class="form-group has-feedback">
                        <label for="slug">Slug</label>
                        <input type="text" class="form-control" placeholder="Leave this field empty to be set automatically" name="slug" id="slug" value="{{ isset($post->slug) ? $post->slug : old('slug') }}"/>
                    </div>
                    <div class="form-group has-feedback">
                        <label for="title">Content *</label>
                        <textarea id="content" name="content" class="textarea form-control" placeholder="Place the content of your article here" rows="30">
                            {{ isset($post->content) ? $post->content : old('content') }}
                        </textarea>
                    </div>
                    <div class="form-group has-feedback">
                        <label for="excerpt">Excerpt</label>
                        <textarea id="excerpt" name="excerpt" class="textarea form-control" placeholder="Place a small part of your article content here" rows="4">{{ isset($post->excerpt) ? $post->excerpt : old('excerpt') }}</textarea>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
        <div class="col-lg-3 col-md-3">
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Status & Author</h3>
                </div><!-- /.box-header -->
                <!-- form start -->

                <div class="box-body">
                    <div class="form-group has-feedback">
                        <?php
                        $selected_draft = $selected_published = '';
                        
                        if(isset($post->status)){
                            if($post->status == 'draft'){
                                $selected_draft = 'selected';
                            } elseif($post->status == 'published'){
                                $selected_published = 'selected';
                            }
                        } else {
                            $old_value = old('status');
                            if($old_value == 'draft'){
                                $selected_draft = 'selected';
                            } elseif($old_value == 'published'){
                                $selected_published = 'selected';
                            }
                        }
                        ?>
                        <select name="status" class="form-control">
                            <option value="draft" {{ $selected_draft }}>Draft</option>
                            <option value="published" {{ $selected_published }}>Published</option>
                        </select>
                    </div>
                    @if(isset($post->user_id))
                    <p class="text-mute">Author: <b>{{ $post->author() }}</b></p>
                    @endif
                    
                    @if(isset($post->status) && $post->status == 'published')
                    <p class="text-mute">Last published: <b>{{ $post->published_at() }}</b></p>
                    @endif
                </div>
            </div>
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Category</h3>
                </div><!-- /.box-header -->
                <!-- form start -->

                <div class="box-body">
                    <div class="form-group has-feedback">
                        @if(!isset($categories) || $categories->isEmpty())
                            You have not created any category. <a href="{{ URL::Route('admin.category.add') }}" target="_blank">Click here</a> to create a new one.
                        @else
                            <?php
                            if(isset($post) && !empty($post)){
                                $post_categories = $post->categories()->get()->pluck('id')->toArray();                            
                            } else {
                                $post_categories = [];
                            }
                            ?>
                            <select name="category[]" class="form-control select2" multiple id="category">
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}" @if(in_array($category->id, $post_categories)) selected @endif>{{ $category->name }}</option>
                                @endforeach
                            </select>
                        @endif
                    </div>
                </div>
            </div>
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Featured Images</h3>
                </div><!-- /.box-header -->
                <!-- form start -->

                <div class="box-body">
                    <div class="form-group has-feedback">
                        @if(empty($post->featured_image) == FALSE && empty($post->thumbnail) == FALSE)
                        <div class="form-group">
                            <img src="{{ $post->thumbnail() }}" class="featured-image"/>
                        </div>
                        @endif
                        <div class="form-group">
                            <label for="featured_image">Upload a new one</label>
                            <input type="file" name="featured_image" id="featured_image" class=""/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" id="post_id" name="post_id" value="{{ isset($post->id) && !empty($post->id) ? $post->id : ''}}">
</form>