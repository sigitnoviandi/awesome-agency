@extends('vendor.adminlte.layouts.app')

@section('htmlheader_title')
    All Articles
@endsection

@section('contentheader_title')
    All Articles
@endsection

@section('contentheader_description')
    All articles
@endsection

@section('page_breadcrumbs')
    {!! Breadcrumbs::render('manage_posts') !!}
@endsection

@section('custom-css')
<!--<link href="{{ asset('/css/contacts.css') }}" rel="stylesheet" type="text/css" />-->
@endsection

@section('main-content')
    <!-- page buttons -->
    <div class="row button-wrapper">
        <div class="col-lg-6 col-xs-12">
            <a href="{{ URL::Route('admin.post.add') }}" class="btn btn-default btn-sm btn-new-post" title="Add a New Article"><i class="fa fa-plus"></i> New Post</a>
        </div>
        <div class="col-lg-6 col-xs-12 action-wrapper">
            <a href="{{ URL::Route('admin.post.filter.clear') }}" class="btn btn-default btn-sm btn-reset-filter pull-right" id="btn-clear-filter">Clear Filter</a>
            <button class="btn btn-default btn-sm btn-search-filter pull-right" id="btn-search"><i class="fa fa-search"></i> Search</button>
        </div>
    </div>
    <!-- end of page buttons -->
    
    <!-- alert -->
    @if(count(Alert::get()) > 0)        
        @foreach (Alert::get() as $alert)
            <div class="alert alert-{{ $alert->class }} alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <p>{{ $alert->message }}</p>
            </div>
        @endforeach                    
    @endif
    <!-- end of alert -->

    <!-- page table -->
    @include('backend.post.partials.table')
    <!-- end of page table -->
    
@endsection

@section('custom-js')
    <script>
        var AJAX_URL = {
           
        };
    </script>
    <script src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>
    <script src="{{ asset('js/table.min.js') }}"></script>
    <script src="{{ asset('js/post.min.js') }}"></script>
@endsection