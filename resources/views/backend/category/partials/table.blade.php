<div class="box table-container">
    <div class="box-header">
        Show 
        <select name="limit" id="filter-limit-options" class="limit-options">
            <option value="10" @if(isset($filter['filter_limit']) && $filter['filter_limit'] == 10) selected @endif>10</option>
            <option value="20" @if(isset($filter['filter_limit']) && $filter['filter_limit'] == 20) selected @endif>20</option>
            <option value="40" @if(isset($filter['filter_limit']) && $filter['filter_limit'] == 40) selected @endif>40</option>
            <option value="50" @if(isset($filter['filter_limit']) && $filter['filter_limit'] == 50) selected @endif>50</option>
            <option value="100" @if(isset($filter['filter_limit']) && $filter['filter_limit'] == 100) selected @endif>100</option>
        </select> baris
        <div class="box-tools">
            @include('backend.pagination.default', ['paginator' => $categories])
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body no-padding">
        <form id="filter-form" method="post" action="{{ URL::Route('admin.category.filter.set') }}">            
            <table class="table table-striped table-hover table-bordered">
                <tr>
                    <th style="width: 2%;">#</th>
                    <th style="width: 70%;">Name</th>
                    <th style="width: 10%;">No. of Articles</th>
                    <th style="width: 15%;">&nbsp;</th>
                </tr>
                <tr class="filter">
                    <td>&nbsp;</td>
                    <td><input type="text" name="filter_name" class="form-control input-sm" placeholder="Search Name" value="{{ isset($filter['filter_name']) ? $filter['filter_name'] : '' }}"/></td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                @if(isset($categories) && !empty($categories))
                    @foreach($categories as $category)
                        <tr>
                            <td>{{ $row_no++ }}</td>
                            <td><a href="{{ URL::Route('admin.category.edit', [$category->id]) }}" title="Edit this Category">{{ $category->name }}</a></td>
                            <td>{{ $category->posts()->count() }} / {{ $category->total_article_in_this_category() }}</td>
                            <td class="action-buttons">
                                <a href="{{ URL::Route('admin.category.remove', [$category->id]) }}" class="btn btn-danger btn-sm btn-remove" data-record-name="{{ $category->name }}" title="Remove Article"><i class="fa fa-trash"></i></a>
                                <a href="{{ URL::Route('admin.category.edit', [$category->id]) }}" class="btn btn-default btn-sm" title="Edit Article"><i class="fa fa-pencil"></i></a>
                                <a href="{{ URL::Route('article.list_by_category', [$category->slug]) }}" class="btn btn-default btn-sm" title="View Article" target="_blank"><i class="fa fa-eye"></i></a>
                            </td>
                        </tr>
                    @endforeach
                @else
                <tr>
                    <td colspan="4">There is no category yet.</td>
                </tr>
                @endif
            </table>
            <input type="hidden" value="10" name="filter_limit" id="filter-limit"/>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
        </form>
    </div>
    <!-- /.box-body -->
    <div class="box-footer clearfix">
        <span class="no-of-records">Showing {{ $first_record_no }}&ndash;{{ $row_no - 1 }} of {{ number_format($categories->total()) }} rows.</span>
        @include('backend.pagination.default', ['paginator' => $categories])
    </div>
</div>
<!-- /.box -->