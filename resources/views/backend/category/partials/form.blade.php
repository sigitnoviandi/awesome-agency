<!-- general form elements -->
<!-- form start -->
<form role="form" id="form_category" action="{{ URL::Route('admin.category.save') }}" method="POST" class="category-form">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-default">
                <div class="box-body action-wrapper">  
                    <span>Changes will NOT be saved before you click the Save button.</span>
                    <a class="btn btn-danger pull-right" id="btn_reset">Reset</a>
                    <a class="btn btn-default pull-right" href="{{ URL::Route('admin.category.manage') }}" id="btn_cancel">Cancel</a>
                    <a class="btn btn-primary pull-right" id="btn_save">Save</a>
                    <span id="loading-indicator" class="loading-indicator pull-right inactive"><i class="fa fa-spinner fa-spin fa-pulse fa-lg"></i></span>
                </div>
            </div>
        </div>
    </div>

    <!-- form start -->
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        @if(isset($category) && !empty($category))
                            Edit Category
                        @else
                            Add a New Category
                        @endif
                    </h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group has-feedback">
                        <label for="name">Name *</label>
                        <input type="text" class="form-control" placeholder="Category Name" name="name" id="name" value="{{ isset($category->name) ? $category->name : old('name') }}"/>
                    </div>
                    <div class="form-group has-feedback">
                        <label for="slug">Slug</label>
                        <input type="text" class="form-control" placeholder="Leave this field empty to be set automatically" name="slug" id="slug" value="{{ isset($category->slug) ? $category->slug : old('slug') }}"/>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>    
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" id="category_id" name="category_id" value="{{ isset($category->id) && !empty($category->id) ? $category->id : ''}}">
</form>