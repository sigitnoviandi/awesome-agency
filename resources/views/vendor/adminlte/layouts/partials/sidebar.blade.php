<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- search form (Optional) -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="{{ trans('adminlte_lang::message.search') }}..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">NAVIGATION</li>
            <!-- Optionally, you can add icons to the links -->
            <li class=""><a href="{{ url('/') }}"><i class='fa fa-home'></i> <span>View Site</span></a></li>
            <li @if(preg_match('/admin\/dashboard/', Request::url())) class="active" @endif><a href="{{ url('admin/dashboard') }}"><i class='fa fa-dashboard'></i> <span>Dashboard</span></a></li>
            <li @if(Request::segment(1) == 'admin' && preg_match('/post/', Request::segment(2))) class="active" @else class="treeview" @endif>
                <a href="#"><i class='fa fa-file'></i> <span>Articles</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li class="{{ preg_match('/post\/add/', Request::url()) ? 'active' : '' }}"><a href="{{ URL::Route('admin.post.add') }}" title="Create a New Article"><i class="fa fa-plus"></i> Add New</a></a></li>
                    <li class="{{ preg_match('/posts\/manage/', Request::url()) ? 'active' : '' }}"><a href="{{ URL::Route('admin.post.manage') }}" title="Manage All Articles"><i class="fa fa-archive"></i> Manage Articles</a></li>
                </ul>
            </li>
            <li @if(Request::segment(1) == 'admin' && preg_match('/categor/', Request::segment(2))) class="active" @else class="treeview" @endif>
                <a href="#"><i class='fa fa-folder-open'></i> <span>Categories</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li class="{{ preg_match('/category\/add/', Request::url()) ? 'active' : '' }}"><a href="{{ URL::Route('admin.category.add') }}" title="Create a New Category"><i class="fa fa-plus"></i> Add New</a></a></li>
                    <li class="{{ preg_match('/category\/manage/', Request::url()) ? 'active' : '' }}"><a href="{{ URL::Route('admin.category.manage') }}" title="Manage Your Categories"><i class="fa fa-folder"></i> Manage Categories</a></a></li>
                </ul>
            </li>
            <li @if(preg_match('/admin\/user\/profile\/edit/', Request::url())) class="active" @endif><a href="{{ url('admin/user/profile/edit') }}"><i class='fa fa-user'></i> <span>Update Profile</span></a></li>
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
