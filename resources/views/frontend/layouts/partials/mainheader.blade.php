<header>
    <div class="collapse bg-dark" id="navbarHeader">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-md-7 py-4">
                    <h4 class="text-white">About</h4>
                    <p class="text-muted">This web page is created for the purpose of job vacancy test.</p>
                    <div>
                        @if(isset($logged_in_user) && !empty($logged_in_user))
                            <a href="{{ url('admin/dashboard') }}">Dashboard</a>
                        @else
                            <a href="{{ url('login') }}">Login</a>
                        @endif
                    </div>
                </div>
                <div class="col-sm-4 offset-md-1 py-4">
                    <h4 class="text-white">Menu</h4>
                    <ul class="list-unstyled">
                        <li><a href="{{ route('home.index') }}" class="text-white">Home</a></li>
                        @if(isset($random_categories) && $random_categories->isEmpty() == FALSE)
                            @foreach($random_categories as $category)
                            <li><a href="{{ route('article.list_by_category', [$category->slug]) }}" class="text-white">{{ $category->name }}</a></li>
                            @endforeach
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="navbar navbar-dark bg-dark shadow-sm">
        <div class="container d-flex justify-content-between">
            <a href="{{ getenv('APP_URL') }}" title="{{ getenv('APP_NAME') }}" class="navbar-brand d-flex align-items-center">
                <img src="{{ asset('img/logo-small.png') }}" alt="Awesome Digital Logo" class="logo-img"/>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarHeader" aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>
    </div>
</header>