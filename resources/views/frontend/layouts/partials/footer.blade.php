<footer class="text-muted">
    <div class="container">
        <p class="text-center">Copyright &copy; {{ date('Y') }} <a href="{{ getenv('APP_URL') }}" title="{{ getenv('APP_NAME') }}">Awesome Digital</a>. All rights reserved.</p>
    </div>
</footer>