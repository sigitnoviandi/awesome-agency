<!DOCTYPE html>

<html lang="en">

@section('htmlheader')
    @include('frontend.layouts.partials.htmlheader')
@show

<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body data-spy="scroll" data-offset="0" data-target="#navigation">
    
<div id="app">
    @include('frontend.layouts.partials.mainheader')
    
    @include('frontend.layouts.partials.content')
    
    @include('frontend.layouts.partials.footer')

</div><!-- app -->

@section('scripts')
    @include('frontend.layouts.partials.scripts')
@show

</body>
</html>