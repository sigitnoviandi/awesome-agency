@extends('frontend.layouts.app')

@section('htmlheader_title')
Awesome Digital - Your awesome digital agency based in Jakarta.
@endsection

@section('main-content')
<main role="main">
    <section class="jumbotron text-center">
        <div class="container">
            <h1 class="jumbotron-heading">Awesome Image Slider</h1>
            <p class="lead text-muted">Place your awesome image slider here to showcase important stuff.</p>
            <p>
                <a href="#" class="btn btn-primary my-2">Main CTA Button</a>
                <a href="#" class="btn btn-secondary my-2">Secondary CTA Button</a>
            </p>
        </div>
    </section>

    <div class="album py-5 bg-light">
        <div class="container">
            @if(empty($articles) || !isset($articles))
                <div class="row">
                    <div class="col-md-12 article-wrapper">
                        <h3>Oops, there is no article yet.</h3>
                    </div>
                </div>
            @else
                <div class="row">
                    @foreach($articles as $article)
                        <div class="col-md-6 article-wrapper">
                            <div class="card mb-4 shadow-sm">
                                <a href="{{ URL::Route('article.single', [$article->slug])}}" title="Read {{ $article->title }}">
                                    <img src="{{ $article->thumbnail() }}" alt="{{ $article->title }} featured image" class="card-img-top"/>
                                </a>
                                <div class="card-body">
                                    <h3 class="article-title"><a href="{{ URL::Route('article.single', [$article->slug])}}" title="Read {{ $article->title }}">{{ $article->title }}</a></h3>
                                    @if(count($article->categories) > 0)
                                        <div class="mb-4">
                                            @foreach($article->categories as $category)
                                                <span class="badge badge-light"></span>
                                                <a href="{{ URL::Route('article.list_by_category', [$category->slug]) }}" class="badge badge-info">{{ $category->name }}</a>
                                            @endforeach
                                        </div>
                                    @endif
                                    <p class="card-text">{{ $article->excerpt() }}</p>
                                    <div class="d-flex justify-content-between align-items-center">
                                        <div class="btn-group">
                                            <a href="{{ URL::Route('article.single', [$article->slug])}}" title="Read {{ $article->title }}" class="btn btn-sm btn-outline-secondary">Read More</a>
                                            @if(isset($logged_in_user) && !empty($logged_in_user))
                                                <a href="{{ URL::Route('admin.post.edit', [$article->id])}}" title="Edit {{ $article->title }}" class="btn btn-sm btn-outline-secondary">Edit</a>
                                            @endif
                                        </div>
                                        <small class="text-muted">Posted by <b>{{ $article->author() }}</b> {{ $article->published_at() }}</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="row">
                    <div class="col-md-12">{{ $articles->links() }}</div>
                </div>
            @endif
        </div>
    </div>

</main>
@endsection

@section('custom-js')

@endsection
