@extends('frontend.layouts.app')

@section('htmlheader_title')
Awesome Digital - {{ $article->title }}
@endsection

@section('main-content')
<main role="main" class="article single">
    <div class="album py-5 bg-light">
        <div class="container">
            <div class="row">
                <div class="col-md-12 mb-4">
                    <div class="card mb-4 shadow-sm">
                        <img src="{{ $article->featured_image() }}" alt="{{ $article->title }} featured image" class="featured-image img-fluid"/>
                        <div class="card-body">
                            <h1 class="article-title">{{ $article->title }}</h1>
                            <p><small class="text-muted">Posted by <b>{{ $article->author() }}</b> {{ $article->published_at() }}</small></p>
                            @if(count($article->categories) > 0)
                                <div class="mb-4">
                                    @foreach($article->categories as $category)
                                        <span class="badge badge-light"></span>
                                        <a href="{{ URL::Route('article.list_by_category', [$category->slug]) }}" class="badge badge-info">{{ $category->name }}</a>
                                    @endforeach
                                </div>
                            @endif
                            <div class="card-text">{!! $article->content !!}</div>
                            @if(isset($logged_in_user) && !empty($logged_in_user))
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="btn-group">
                                    <a href="{{ URL::Route('admin.post.edit', [$article->id])}}" title="Edit {{ $article->title }}" class="btn btn-sm btn-outline-secondary">Edit</a>
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h2>Related Articles</h2>
                    @if($related_articles->isEmpty())
                        <h3>Oops, there is no related article.</h3>
                    @else
                        <div class="row">
                            @foreach($related_articles as $related_article)
                                <div class="col-md-4 article-wrapper">
                                    <div class="card mb-4 shadow-sm">
                                        <a href="{{ URL::Route('article.single', [$related_article->slug])}}" title="Read {{ $related_article->title }}">
                                            <img src="{{ $related_article->thumbnail() }}" alt="{{ $related_article->title }} featured image" class="card-img-top"/>
                                        </a>
                                        <div class="card-body">
                                            <h3 class="article-title"><a href="{{ URL::Route('article.single', [$related_article->slug])}}" title="Read {{ $related_article->title }}">{{ $related_article->title }}</a></h3>
                                            <p><small class="text-muted">Posted by <b>{{ $related_article->author() }}</b> {{ $related_article->published_at() }}</small></p>
                                            @if(count($related_article->categories) > 0)
                                                <div class="mb-4">
                                                    @foreach($related_article->categories as $category)
                                                        <span class="badge badge-light"></span>
                                                        <a href="{{ URL::Route('article.list_by_category', [$category->slug]) }}" class="badge badge-info">{{ $category->name }}</a>
                                                    @endforeach
                                                </div>
                                            @endif
                                            <p class="card-text">{{ $related_article->excerpt() }}</p>
                                            <div class="d-flex justify-content-between align-items-center">
                                                <div class="btn-group">
                                                    <a href="{{ URL::Route('article.single', [$related_article->slug])}}" title="Read {{ $related_article->title }}" class="btn btn-sm btn-outline-secondary">Read More</a>
                                                    @if(isset($logged_in_user) && !empty($logged_in_user))
                                                        <a href="{{ URL::Route('admin.post.edit', [$related_article->id])}}" title="Edit {{ $related_article->title }}" class="btn btn-sm btn-outline-secondary">Edit</a>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

</main>
@endsection

@section('custom-js')

@endsection
