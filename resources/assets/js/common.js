/*
 *   Common JS Library
 *   
 *   @author     : Sigit Noviandi
 *   @since      : November 2018
 */

$.ajaxPrefilter(function (options, originalOptions, xhr) {
    var token = Laravel.csrfToken;

    if (token) {
        return xhr.setRequestHeader('X-CSRF-TOKEN', token);
    }
});

jQuery(function () {
    // init icheck
    $('input[type="checkbox"].icheck.square-blue, input[type="radio"].icheck.square-blue').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue'
    });    
});

function show_loading_indicator(){
    $('#loading-indicator').show();
}

function hide_loading_indicator(){
    $('#loading-indicator').hide();
}