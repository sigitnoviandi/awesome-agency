/* 
 * Category page Common JS Library
 * 
 * @author      : Sigit Noviandi
 * @since       : November 2018
 */

jQuery(function ($) {
    if (/admin\/user\/profile\/edit/.test(window.location)) {
        User.Profile.Form.init();
    }
});

var User = {
    Profile: {
        Form: {
            init: function () {

                hide_loading_indicator();

                $('#btn_reset').on('click', function(){
                    $('#form_profile')[0].reset();
                });

                $('#btn_save').on('click', function(){
                    show_loading_indicator();
                    $('#form_profile').submit();
                });
            }
        }
    }
};