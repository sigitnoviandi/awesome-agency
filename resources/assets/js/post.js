/* 
 * Article/post page Common JS Library
 * 
 * @author      : Sigit Noviandi
 * @since       : November 2018
 */

jQuery(function ($) {
    if (/admin\/posts\/manage/.test(window.location)) {
        Post.Manage.init();
    } else if (/admin\/post\/edit/.test(window.location) || /admin\/post\/add/.test(window.location)) {
        Post.Form.init();
    }
});

var Post = {
    Manage: {
        init: function () {
            // init table
            SN_Table.init();
            
            $('.post-category').on('click', function(){
                $('#filter_category').val($(this).data('category'));
                $('#btn-search').trigger('click');
            });
            
            $('.post-status').on('click', function(){
                $('#filter_status').val($(this).data('status'));
                $('#btn-search').trigger('click');
            });
            
            $('.post-author').on('click', function(evt){
                evt.preventDefault();
                $('#filter_author').val($(this).data('author'));
                $('#btn-search').trigger('click');
            });
        }
    },

    Form: {
        init: function () {
            
            hide_loading_indicator();
            
            $('#content').wysihtml5();
            
            $('.select2').select2({multiple:true});
            
            if(categories_values && categories_values.length > 0){
                $('#category').val(categories_values);
                $('#category').trigger('change');
            }
            
            $('#btn_reset').on('click', function(){
                $('#form_post')[0].reset();
            });
            
            $('#btn_save').on('click', function(){
                show_loading_indicator();
                $('#form_post').submit();
            });
            
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        }
    }
};