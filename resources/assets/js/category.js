/* 
 * Category page Common JS Library
 * 
 * @author      : Sigit Noviandi
 * @since       : November 2018
 */

jQuery(function ($) {
    if (/admin\/categories\/manage/.test(window.location)) {
        Category.Manage.init();
    } else if (/admin\/category\/edit/.test(window.location) || /admin\/category\/add/.test(window.location)) {
        Category.Form.init();
    }
});

var Category = {
    Manage: {
        init: function () {
            // init table
            SN_Table.init();
        }
    },

    Form: {
        init: function () {
            
            hide_loading_indicator();
            
            $('#btn_reset').on('click', function(){
                $('#form_category')[0].reset();
            });
            
            $('#btn_save').on('click', function(){
                show_loading_indicator();
                $('#form_category').submit();
            });
            
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        }
    }
};