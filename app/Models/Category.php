<?php

/*
 * Category Model
 * A database model to connect to the table categories.
 *
 * @author      : Sigit Noviandi
 * @since       : November 2018
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Support\Facades\Auth;

class Category extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'categories';
    
    public static function search($filter = array(), $limit = 10) {
        $logged_in_user = Auth::user();
        
        $categories = new Category();
        
        if (is_array($filter) && !empty($filter)) {
            foreach($filter as $key => $value){
                $filter[$key] = trim($value);
            }
            if (isset($filter['filter_name']) && $filter['filter_name'] != '') {
                $categories = $categories->where('name', 'like', '%' . $filter['filter_name'] . '%');
            }
        }
        
        $categories = $categories->orderBy('name', 'ASC');
        
        $categories = $categories->paginate($limit);

        return $categories;
    }
    
    public static function available_categories($name_only = FALSE){
        $logged_in_user = Auth::user();
        
        $categories = new Category();
        $categories = $categories->orderBy('name', 'ASC')->get();
        
        if($categories->isEmpty()){
            return TRUE;
        }
        
        if($name_only){
            return $categories->pluck('name')->toArray();
        }
        
        return $categories;
    }
    
    public function total_article_in_this_category(){
        $category_name = $this->name;
        
        return Post::whereHas('categories', function ($query) use($category_name) {
            $query->where('name', 'like', '%' . $category_name . '%');
        })->get()->count();
    }

    /**
     * Get the user that owns the documents
     */
    public function user() {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
    
    /**
     * Get the documents the subskills belongs to
     */
    public function posts() {
        return $this->belongsToMany('App\Models\Post', 'posts_categories')->withTimestamps();
    }

}
