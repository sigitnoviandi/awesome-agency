<?php

/*
 * User Model
 * A database model to connect to the table users.
 *
 * @author      : Sigit Noviandi
 * @since       : November 2018
 */

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    /**
     * Get the categories belong to the user
     */
    public function categories() {
        return $this->hasMany('App\Models\Category');
    }
    
    /**
     * Get the posts belong to the user
     */
    public function posts() {
        return $this->hasMany('App\Models\Post');
    }
}
