<?php

/*
 * Post Model
 * A database model to connect to the table posts.
 *
 * @author      : Sigit Noviandi
 * @since       : November 2018
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Support\Carbon;

use Illuminate\Support\Facades\Auth;

class Post extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'posts';
    
    public static function search($filter = array(), $limit = 10) {

        $posts = new Post();
        if (is_array($filter) && !empty($filter)) {
            foreach($filter as $key => $value){
                $filter[$key] = trim($value);
            }
            if (isset($filter['filter_title']) && $filter['filter_title'] != '') {
                $posts = $posts->where('title', 'like', '%' . $filter['filter_title'] . '%');
            }
            if (isset($filter['filter_author']) && $filter['filter_author'] != '') {
                $authors = User::where('name', 'like', '%' . $filter['filter_author'] . '%')->get();
                if($authors->isEmpty() == FALSE){
                    $authors = $authors->pluck('id')->toArray();
                }
                $posts = $posts->whereIn('user_id', $authors);
            }
            if (isset($filter['filter_status']) && $filter['filter_status'] != '') {
                $posts = $posts->where('status', $filter['filter_status']);
            }
            if (isset($filter['filter_category']) && $filter['filter_category'] != '') {
                
                $posts = $posts->whereHas('categories', function ($query) use($filter) {
                    $query->where('name', 'like', '%' . $filter['filter_category'] . '%');
                });
            }
        }
       
        $posts = $posts->orderBy('created_at', 'DESC');
        
        $posts = $posts->paginate($limit);

        return $posts;
    }
    
    public static function published_posts($category = 'all', $per_page = 10, $page = 1, $order_by = 'published_at', $order = 'DESC'){
        $posts = new Post();
        $posts = $posts->where('status', 'published');
        if($category != 'all' && is_int($category)){
            $categories_ids = [$category];
            $posts = $posts->whereHas('categories', function ($query) use($categories_ids) {
                $query->whereIn('categories.id', $categories_ids);
            });
        }
        $posts = $posts->orderBy($order_by, $order);
        $posts = $posts->paginate($per_page);
        
        return $posts;
    }
    
    public function excerpt(){
        $excerpt = $this->excerpt;
        
        if(empty($excerpt)){
            $excerpt = strip_tags($this->content);
            $excerpt = substr($excerpt, 0, 200);
        }
        
        return $excerpt;
    }
    
    public function author(){
        return $this->user->name;
    }
    
    public function published_at(){
        if(empty($this->published_at)){
            return '-';
        }
        $published_at = Carbon::createFromFormat('Y-m-d H:i:s', $this->published_at);
        return $published_at->diffForHumans() ;
    }
    
    public function related_articles(){
        $posts = new Post();
        $categories_ids = $this->categories()->get()->pluck('id')->toArray();
        $posts = $posts->whereHas('categories', function ($query) use($categories_ids) {
            $query->whereIn('categories.id', $categories_ids);
        });
        $related_articles = $posts->where('posts.id', '<>', $this->id)->where('status', 'published')->orderBy('published_at', 'DESC')->limit(3)->get();
        
        return $related_articles;
    }
    
    public function featured_image(){
        if(empty($this->featured_image)){
            $featured_image = asset('img/no-featured-image.jpg');
        } else {
            $featured_image = asset('storage/images/' . $this->featured_image );
        }

        return $featured_image;
    }
    
    public function thumbnail(){
        if(empty($this->thumbnail)){
            $thumbnail = asset('svg/no-thumbnail.svg');
        } else {
            $thumbnail = asset('storage/thumbnails/' . $this->thumbnail );
        }
        
        return $thumbnail;
    }
    
    public function owned(){
        $logged_in_user = Auth::user();
        
        if(empty($logged_in_user)){
            return FALSE;
        }
        
        if($this->user_id == $logged_in_user->id){
            return TRUE;
        }
        
        return FALSE;
    }
    
    /**
     * Get the user that owns the documents
     */
    public function user() {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
    
    /**
     * Get the categories belong to the user
     */
    public function categories() {
        return $this->belongsToMany('App\Models\Category', 'posts_categories')->withTimestamps();
    }
    
}
