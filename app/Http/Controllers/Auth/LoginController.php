<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;

use App\Models\User;

class LoginController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Login Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles authenticating users for the application and
      | redirecting them to your home screen. The controller uses a trait
      | to conveniently provide its functionality to your applications.
      |
     */

use AuthenticatesUsers;

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm() {
        return view('adminlte::auth.login');
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function twitter_login() {
        
    }

    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirect_to_provider($social_media) {
        return Socialite::driver($social_media)->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handle_provider_callback($social_media) {
        $social_media_user = Socialite::driver($social_media)->user();
        if(empty($social_media_user) == FALSE){
            $email = $social_media_user->getEmail();
            if(empty($email) == FALSE){
                $user = User::where('email', $email)->first();
                if(empty($user) == FALSE){
                    Auth::login($user);
                    return redirect(URL::Route('admin.dashboard'));
                } else {
                    $user = new User();
                    $user->name = $social_media_user->getName();
                    $user->email = $email;
                    $user->password = \Illuminate\Support\Facades\Hash::make(str_random(10));
                    $user->save();
                    
                    Auth::login($user);
                    return redirect(URL::Route('admin.dashboard'));
                }
            }
        }
        
        return redirect('login');
    }

}
