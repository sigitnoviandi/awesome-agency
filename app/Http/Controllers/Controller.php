<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

class Controller extends BaseController {

    use AuthorizesRequests,
        DispatchesJobs,
        ValidatesRequests;

    public function __construct() {
        $this->middleware(function ($request, $next) {
            $logged_in_user = Auth::user();
            if (empty($logged_in_user) == FALSE) {
                View::share('logged_in_user', $logged_in_user);
            }
            return $next($request);
        });
        
        $random_categories = \App\Models\Category::inRandomOrder()->limit('4')->get();
        
        View::share('random_categories', $random_categories);
    }

}
