<?php

namespace App\Http\Controllers\Back;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Cartalyst\Alerts\Laravel\Facades\Alert;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;

use App\Http\Controllers\Controller;

use App\Models\Post;
use App\Models\Category;

class PostController extends Controller {
    
    protected $session_filter = 'post_filter';
    protected $user;
    
    public function __construct() {
        parent::__construct();
    }
    
    public function index(Request $request){
        $limit = 10;
        
        ## check the previous request to see if we should apply the filter
        $previous_url = URL::previous();
        if($previous_url != '' && !preg_match('/admin\/posts\/manage/i', $previous_url)){
            $request->session()->forget($this->session_filter);
        }
        
        $filter = $request->session()->get($this->session_filter);
        if(isset($filter['filter_limit']) && $filter['filter_limit'] != '' && $filter['filter_limit'] > 0){
            $limit = $filter['filter_limit'];
        } else {
            $filter['filter_limit'] = $limit;
        }
        
        $posts = Post::search($filter, $limit);
        
        if($posts->currentPage() == 1){
            $first_record_no = 1;
        } else {
            $first_record_no = (($posts->currentPage() - 1) * $limit) + 1;
        }
        
        return view('backend.post.manage', ['posts' => $posts, 'first_record_no' => $first_record_no, 'row_no' => $first_record_no, 'filter' => $filter]);
    }
    
    public function add(Request $request){
        $categories = Category::available_categories();
        
        return view('backend.post.add', ['categories' => $categories])->withEncryptedCsrfToken(Crypt::encrypt(csrf_token()));
    }
    
    public function edit($post_id, Request $request){
        
        $post = Post::find($post_id);
        
        if(empty($post)){
            Alert::error('We could not find the article you specified. It may have been removed or you selected an invalid article.');
            
            return redirect(URL::Route('admin.post.manage'));
        }
        
        $categories = Category::available_categories();
        
        return view('backend.post.edit', ['post' => $post, 'categories' => $categories])->withEncryptedCsrfToken(Crypt::encrypt(csrf_token()));
    }
    
    public function save(Request $request){
        $post_id = $request->input('post_id');
        
        $action = 'add';
        
        if(empty($post_id)){
            $post = new Post();
        } else {
            $post = Post::find($post_id);
            
            if(empty($post)){
                Alert::error('We could not find the article you are trying to update.');
                return redirect(URL::Route('admin.post.manage'));
            }
            
            $action = 'edit';
        }
        
        $validator = $this->post_save_validator($request->post(), $post)->validate();
        
        $data = $request->post();
        
        $post->title = $data['title'];
        $post->status = $data['status'];
        if($data['status'] == 'published'){
            $post->published_at = date('Y-m-d H:i:s');
        }
        $post->content = nl2br($data['content']);
        
        ## slug
        if(isset($data['slug'])){
            $post->slug = $data['slug'];
        } else {
            if($action == 'add'){
                $post->slug = str_slug($data['title']);
            }
        }
      
        ## excerpt
        if(isset($data['excerpt'])){
            $post->excerpt = $data['excerpt'];
        } else {
            if($action == 'add'){
                $post->excerpt = substr(strip_tags($data['content']), 0, 200);
            }
        }

        ## featured image
        $remove_old_featured_image = FALSE;
        if ($request->hasFile('featured_image')) {
            if ($request->file('featured_image')->isValid()) {
            
            } else {
                Alert::error('The file you are uploading is invalid.');
                return redirect(URL::Route('admin.post.edit', [$post_id]));
            }

            $extension = $request->featured_image->extension();

            $filename = str_random(35) . '.' . $extension;
            $path = $request->featured_image->storeAs('images', $filename);
            $filename = basename($path);

            if (empty($filename)) {
                Alert::error('Failed uploading the featured image. Please try again later.');
                return redirect(URL::Route('admin.post.edit', [$post_id]));
            }
            
            ## resize image
            $img = Image::make(Storage::path($path));
            $img->resize(800, NULL, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save(Storage::path('thumbnails/' . $filename));
            
            ## remove old images
            if($action == 'edit'){
                $remove_old_featured_image = TRUE;
                $old_featured_image = $post->featured_image;
                $old_thumbnail = $post->thumbnail;
            }
            
            $post->featured_image = $filename;
            $post->thumbnail = $filename;
        } 
        
        if($action == 'add'){
            $logged_in_user = Auth::user();
            $logged_in_user->posts()->save($post);
        } else {
            $post->save();
        }
        
        ## category
        $post->categories()->sync($data['category']);
        
        if($remove_old_featured_image == TRUE){
            Storage::delete('images/' . $old_featured_image);
            Storage::delete('thumbnails/' . $old_thumbnail);
        }
        
        if($action == 'add'){
            Alert::foo('Your article has been saved.');
            return redirect(URL::Route('admin.post.manage'));
        } else {
            Alert::foo('Your article has been updated.');
            return redirect(URL::Route('admin.post.edit', [$post_id]));
        }
    }
    
    public function remove($post_id){
        $post = Post::find($post_id);
        if(empty($post)){
            Alert::error('We could not find the article you are trying to remove.');
            return redirect(URL::Route('admin.post.manage'));
        }
        
        $post_title = $post->title;
        
        // detach the subskills
        $post->categories()->detach();
        
        // now really remove the user
        $result = $post->delete();
        
        if($result){
            Alert::foo('"' . $post_title . '" has been removed.');
            return redirect(URL::Route('admin.post.manage'));
        }
        
        Alert::error('We cannot remove the article at this moment. Please try again later.');
        return redirect(URL::Route('admin.post.manage'));
    }
    
    /**
     * Get a validator for an incoming post save request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function post_save_validator(array $data, $current_post = NULL) {
        $current_post_id = $current_post->id;
        $validator_array = [
            'title' => [
                'required',
                'max:191',
                Rule::unique('posts')->where(function ($query) use($current_post_id) {
                    return $query->where('id', '<>', $current_post_id);
                })
            ],
            'content' => 'required',
            'category' => 'required',
            'avatar' => 'mimes:jpeg,jpg,png|max:2048',
        ];

        if(empty($current_post) == TRUE){
            $validator_array['featured_image'] = 'required|mimes:jpeg,jpg,png|max:2048';
        }
        
        $validator = Validator::make($data, $validator_array);
        
        return $validator;
    }
    
    public function set_filter(Request $request){
        $input = $request->all();
        unset($input['_token']);
        $request->session()->put($this->session_filter, $input);
        
        return redirect(URL::Route('admin.post.manage'));
    }
    
    public function clear_filter(Request $request){        
        $request->session()->forget($this->session_filter);
        
        return redirect(URL::Route('admin.post.manage'));
        
    }
}
