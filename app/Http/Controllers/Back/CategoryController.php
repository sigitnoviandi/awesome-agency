<?php

namespace App\Http\Controllers\Back;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Cartalyst\Alerts\Laravel\Facades\Alert;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;

use App\Models\Post;
use App\Models\Category;

class CategoryController extends Controller {
    
    protected $session_filter = 'category_filter';
    protected $user;
    
    public function __construct() {
        parent::__construct();
    }
    
    public function index(Request $request){
        $limit = 10;
        
        ## check the previous request to see if we should apply the filter
        $previous_url = URL::previous();
        if($previous_url != '' && !preg_match('/admin\/categories\/manage/i', $previous_url)){
            $request->session()->forget($this->session_filter);
        }
        
        $filter = $request->session()->get($this->session_filter);
        if(isset($filter['filter_limit']) && $filter['filter_limit'] != '' && $filter['filter_limit'] > 0){
            $limit = $filter['filter_limit'];
        } else {
            $filter['filter_limit'] = $limit;
        }
        
        $categories = Category::search($filter, $limit);
        
        if($categories->currentPage() == 1){
            $first_record_no = 1;
        } else {
            $first_record_no = (($categories->currentPage() - 1) * $limit) + 1;
        }
        
        return view('backend.category.manage', ['categories' => $categories, 'first_record_no' => $first_record_no, 'row_no' => $first_record_no, 'filter' => $filter]);
    }
    
    public function add(Request $request){        
        return view('backend.category.add')->withEncryptedCsrfToken(Crypt::encrypt(csrf_token()));
    }
    
    public function edit($category_id, Request $request){
        
        $category = Category::find($category_id);
        
        if(empty($category)){
            Alert::error('We could not find the category you specified. It may have been removed or you selected an invalid category.');
            
            return redirect(URL::Route('admin.category.manage'));
        }
        
        return view('backend.category.edit', ['category' => $category])->withEncryptedCsrfToken(Crypt::encrypt(csrf_token()));
    }
    
    public function save(Request $request){
        $category_id = $request->input('category_id');
        
        $action = 'add';
        
        if(empty($category_id)){
            $category = new Category();
        } else {
            $category = Category::find($category_id);
            
            if(empty($category)){
                Alert::error('We could not find the category you are trying to update.');
                return redirect(URL::Route('admin.category.manage'));
            }
            
            $action = 'edit';
        }
        
        $validator = $this->category_save_validator($request->post(), $category)->validate();
        
        $data = $request->post();
        
        $category->name = $data['name'];
        
        ## slug
        if(isset($data['slug'])){
            $category->slug = $data['slug'];
        } else {
            if($action == 'add'){
                $category->slug = str_slug($data['name']);
            }
        }
      
        if($action == 'add'){
            $logged_in_user = Auth::user();
            $logged_in_user->categories()->save($category);
        } else {
            $category->save();
        }
        
        if($action == 'add'){
            Alert::foo('Your category has been saved.');
            return redirect(URL::Route('admin.category.manage'));
        } else {
            Alert::foo('Your category has been updated.');
            return redirect(URL::Route('admin.category.edit', [$category_id]));
        }
    }
    
    public function remove($category_id){
        $category = Category::find($category_id);
        if(empty($category)){
            Alert::error('We could not find the article you are trying to remove.');
            return redirect(URL::Route('admin.post.manage'));
        }
        
        $category_name = $category->name;
        
        // detach the subskills
        $category->posts()->detach();
        
        // now really remove the user
        $result = $category->delete();
        
        if($result){
            Alert::foo('"' . $category_name . '" has been removed.');
            return redirect(URL::Route('admin.category.manage'));
        }
        
        Alert::error('We cannot remove the category at this moment. Please try again later.');
        return redirect(URL::Route('admin.category.manage'));
    }
    
    /**
     * Get a validator for an incoming category save request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function category_save_validator(array $data, $current_category = NULL) {
        $logged_in_user_id = Auth::id();
        
        if(empty($current_category)){
            $current_category_id = '';
        } else {
            $current_category_id = $current_category->id;
        }
        
        $validator_array = [
            'name' => [
                'required',
                'max:191',
                Rule::unique('categories')->where(function ($query) use($logged_in_user_id, $current_category_id) {
                    return $query->where('id', '<>', $current_category_id);
                })
            ]
        ];

        $validator = Validator::make($data, $validator_array);
        
        return $validator;
    }
    
    public function set_filter(Request $request){
        $input = $request->all();
        unset($input['_token']);
        $request->session()->put($this->session_filter, $input);
        
        return redirect(URL::Route('admin.category.manage'));
    }
    
    public function clear_filter(Request $request){        
        $request->session()->forget($this->session_filter);
        
        return redirect(URL::Route('admin.category.manage'));
        
    }
}
