<?php

namespace App\Http\Controllers\Back;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Cartalyst\Alerts\Laravel\Facades\Alert;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use App\Http\Controllers\Controller;

use App\Models\Post;
use App\Models\Category;

class UserController extends Controller {
    
    protected $session_filter = 'post_filter';
    protected $user;
    
    public function __construct() {
        parent::__construct();
    }
    
    public function profile_form(Request $request){
        
        $user = Auth::user();
        
        return view('backend.user.profile_edit', ['user' => $user])->withEncryptedCsrfToken(Crypt::encrypt(csrf_token()));
    }
    
    public function profile_save(Request $request){
        $user = Auth::user();
        
        $validator = $this->profile_save_validator($request->post(), $user)->validate();
        
        $data = $request->post();
        
        if(isset($data['new_password']) && !empty($data['new_password']) && $data['new_password'] != ''){
            if(Hash::check($data['old_password'], $user->password) == FALSE){
                Alert::error('The old password you entered does not match the one we have in the database.');
                return redirect(URL::Route('admin.user.profile.edit'));
            }
        }
        
        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->password = Hash::make($data['new_password']);
        
        $user->save();
        
        Alert::foo('Your profile has been updated.');
        return redirect(URL::Route('admin.user.profile.edit'));
    }
    
    
    /**
     * Get a validator for an incoming user profile save request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function profile_save_validator(array $data, $current_user = NULL) {
        $validator_array = [
            'name' => 'required|max:191',
            'email' => 'required|email|max:191',
        ];
        
        $validator = Validator::make($data, $validator_array);
        
        $validator->sometimes(['email'], 'unique:users', function ($input) use($current_user) {
            return (isset($input->email) && !empty($input->email) && $input->email != '' && $input->email != $current_user->email);
        });
        
        $validator->sometimes(['old_password'], 'required', function ($input) {
            return (isset($input->new_password) && !empty($input->new_password) && $input->new_password != '');
        });  
        
        $validator->sometimes(['new_password'], 'min:6|confirmed', function ($input) {
            return (isset($input->new_password) && !empty($input->new_password) && $input->new_password != '');
        });        
        
        return $validator;
    }
}
