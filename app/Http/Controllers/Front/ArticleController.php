<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Post;
use App\Models\Category;

class ArticleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        parent::__construct();
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index($slug){
        $article = Post::where('slug', $slug)->where('status', 'published')->first();
        
        if(empty($article)){
            abort(404);
        }
        
        $related_articles = $article->related_articles();
        
        return view('frontend.article.single', ['article' => $article, 'related_articles' => $related_articles]);
    }
    
    public function list_by_category($slug){
        $category = Category::where('slug', $slug)->first();
        
        if(empty($category)){
            abort(404);
        }
        
        $articles = Post::published_posts($category->id);
        
        return view('frontend.article.list', ['articles' => $articles, 'category' => $category]);
    }
}
