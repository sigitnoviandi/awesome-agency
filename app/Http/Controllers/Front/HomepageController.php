<?php

namespace App\Http\Controllers\Front;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Post;

/**
 * Class HomepageController
 * @package App\Http\Controllers
 */
class HomepageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        parent::__construct();
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index(){
        $articles = Post::published_posts();
        
        return view('frontend.homepage', ['articles' => $articles]);
    }
}